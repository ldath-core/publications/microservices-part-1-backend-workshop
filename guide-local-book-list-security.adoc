You can check if this service is passing security tests by running script `local-scan.sh`:

[source,shell]
----
./local-scan.sh
----

Your output should look similar to this:

[source,text]
----
[+] Building 21.5s (14/14) FINISHED                                                                                                                                                                                                                                                                           docker:colima
 => [internal] load .dockerignore                                                                                                                                                                                                                                                                                      0.0s
 => => transferring context: 349B                                                                                                                                                                                                                                                                                      0.0s
 => [internal] load build definition from multi-stage.Dockerfile                                                                                                                                                                                                                                                       0.0s
 => => transferring dockerfile: 516B                                                                                                                                                                                                                                                                                   0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                                                                                                                                                                                                                                                       0.8s
 => [internal] load metadata for docker.io/library/golang:latest                                                                                                                                                                                                                                                       0.8s
 => [builder 1/4] FROM docker.io/library/golang:latest@sha256:969349b8121a56d51c74f4c273ab974c15b3a8ae246a5cffc1df7d28b66cf978                                                                                                                                                                                         0.0s
 => [internal] load build context                                                                                                                                                                                                                                                                                      3.9s
 => => transferring context: 481.27MB                                                                                                                                                                                                                                                                                  3.9s
 => [stage-1 1/4] FROM docker.io/library/alpine:latest@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd                                                                                                                                                                                         0.0s
 => CACHED [builder 2/4] WORKDIR /go/src/gitlab.com/mobica-workshops/examples/go/gorilla/book-list                                                                                                                                                                                                                     0.0s
 => [builder 3/4] ADD . ./                                                                                                                                                                                                                                                                                             1.3s
 => [builder 4/4] RUN ./build.sh linux                                                                                                                                                                                                                                                                                15.2s
 => CACHED [stage-1 2/4] RUN mkdir -p "/var/application"                                                                                                                                                                                                                                                               0.0s
 => CACHED [stage-1 3/4] COPY public /var/application/public                                                                                                                                                                                                                                                           0.0s
 => CACHED [stage-1 4/4] COPY --from=builder /go/src/gitlab.com/mobica-workshops/examples/go/gorilla/book-list/book-list /bin/book-list                                                                                                                                                                                0.0s
 => exporting to image                                                                                                                                                                                                                                                                                                 0.0s
 => => exporting layers                                                                                                                                                                                                                                                                                                0.0s
 => => writing image sha256:c4a86acfaea5a5121554ff7a8738baaca1672a4443951559935f71abd176494c                                                                                                                                                                                                                           0.0s
 => => naming to docker.io/library/book-list:latest                                                                                                                                                                                                                                                                    0.0s
Version: 0.51.1
2024-06-06T15:46:28+02:00	INFO	Removing artifact caches...
2024-06-06T15:46:29+02:00	INFO	Vulnerability scanning is enabled
2024-06-06T15:46:29+02:00	INFO	Secret scanning is enabled
2024-06-06T15:46:29+02:00	INFO	If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2024-06-06T15:46:29+02:00	INFO	Please see also https://aquasecurity.github.io/trivy/v0.51/docs/scanner/secret/#recommendation for faster secret detection
2024-06-06T15:46:29+02:00	INFO	Detected OS	family="alpine" version="3.20.0"
2024-06-06T15:46:29+02:00	WARN	This OS version is not on the EOL list	family="alpine" version="3.20"
2024-06-06T15:46:29+02:00	INFO	[alpine] Detecting vulnerabilities...	os_version="3.20" repository="3.20" pkg_num=14
2024-06-06T15:46:29+02:00	INFO	Number of language-specific files	num=1
2024-06-06T15:46:29+02:00	INFO	[gobinary] Detecting vulnerabilities...

book-list:latest (alpine 3.20.0)

Total: 0 (MEDIUM: 0, HIGH: 0, CRITICAL: 0)


bin/book-list (gobinary)

Total: 5 (MEDIUM: 5, HIGH: 0, CRITICAL: 0)

┌──────────────────────────────┬─────────────────────┬──────────┬────────┬───────────────────┬───────────────┬───────────────────────────────────────────────────────────┐
│           Library            │    Vulnerability    │ Severity │ Status │ Installed Version │ Fixed Version │                           Title                           │
├──────────────────────────────┼─────────────────────┼──────────┼────────┼───────────────────┼───────────────┼───────────────────────────────────────────────────────────┤
│ github.com/jackc/pgproto3/v2 │ CVE-2024-27304      │ MEDIUM   │ fixed  │ v2.3.2            │ 2.3.3         │ pgx: SQL Injection via Protocol Message Size Overflow     │
│                              │                     │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2024-27304                │
│                              ├─────────────────────┤          │        │                   │               ├───────────────────────────────────────────────────────────┤
│                              │ GHSA-7jwh-3vrq-q3m8 │          │        │                   │               │ pgproto3 SQL Injection via Protocol Message Size Overflow │
│                              │                     │          │        │                   │               │ https://github.com/advisories/GHSA-7jwh-3vrq-q3m8         │
├──────────────────────────────┼─────────────────────┤          │        ├───────────────────┼───────────────┼───────────────────────────────────────────────────────────┤
│ github.com/jackc/pgx/v4      │ CVE-2024-27289      │          │        │ v4.18.1           │ 4.18.2        │ pgx: SQL Injection via Line Comment Creation              │
│                              │                     │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2024-27289                │
│                              ├─────────────────────┤          │        │                   │               ├───────────────────────────────────────────────────────────┤
│                              │ CVE-2024-27304      │          │        │                   │               │ pgx: SQL Injection via Protocol Message Size Overflow     │
│                              │                     │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2024-27304                │
│                              ├─────────────────────┤          │        │                   │               ├───────────────────────────────────────────────────────────┤
│                              │ GHSA-7jwh-3vrq-q3m8 │          │        │                   │               │ pgproto3 SQL Injection via Protocol Message Size Overflow │
│                              │                     │          │        │                   │               │ https://github.com/advisories/GHSA-7jwh-3vrq-q3m8         │
└──────────────────────────────┴─────────────────────┴──────────┴────────┴───────────────────┴───────────────┴───────────────────────────────────────────────────────────┘
----