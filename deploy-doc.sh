#!/usr/bin/env bash
mkdir public
cp -R images public/
cp microservices-backend-local-development-guide.html public/index.html
cp microservices-backend-local-development-guide.pdf public/
