= Backend local development with Docker and K3s
Arkadiusz Tułodziecki <arkadiusz.tulodziecki@mobica.com>
1.2, Jun 7, 2024: Microservices Backend Local Development GUIDE
:toc: left
:toclevels: 2
:source-highlighter: rouge
:encoding: utf-8
:icons: font
:url-quickref: https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/
:experimental:

ifdef::backend-html5[]
PDF version of this article:
link:microservices-backend-local-development-guide.pdf/[microservices-backend-local-development-guide.pdf]
endif::[]

[.lead]
Solution which will be proposed during these workshops was created to simplify full process of developing applications. Main target of it is to reduce development time lost to play with the infrastructure in all teams: development, qa, and operations.

== Agenda

include::overview-agenda.adoc[]

== Requirements

include::overview-requirements.adoc[]

=== Why those requirements

include::overview-requirements-description.adoc[]

== Microservices Overview

=== Architecture

include::overview-architecture.adoc[]

=== Automation

include::overview-automation.adoc[]

==== Build

include::overview-automation-build.adoc[]

==== Documentation

include::overview-automation-documentation.adoc[]

==== Local Configuration

include::overview-automation-local-ansible.adoc[]

==== Local Orchestration

include::overview-automation-local-compose.adoc[]

==== Local Testing

include::overview-automation-local-tests.adoc[]

==== Local Scanning

include::overview-automation-local-scans.adoc[]

==== Local Deployment

include::overview-automation-deployment.adoc[]

==== CI/CD

include::overview-automation-ci-gitlab.adoc[]

==== Summary

include::overview-automation-summary.adoc[]

=== Configuration

include::overview-configuration.adoc[]

=== Service Skeleton

include::overview-templates.adoc[]

=== Readiness Checklist

include::overview-rediness-checklist.adoc[]

==== Section by Section example

include::overview-rediness-checklist-description.adoc[]

=== Documentation

include::overview-documentation.adoc[]

=== Services

include::overview-services.adoc[]

=== Mockups

include::overview-mockups.adoc[]

=== Application Monitoring

include::overview-application-monitoring.adoc[]

=== Application Security

include::overview-application-security.adoc[]

== Local Environment

Now let's start practical part of this workshop.

[IMPORTANT]
In case there will be any problems with workshop instructions, and you will find yourself blocked somehow -
than feel free to create issues ticket by using this link:
https://gitlab.com/mobica-workshops/documentation/microservices-backend-local-development-guide/-/issues
and using button: btn:[New issue]. If you are on live workshop just ask a question.

=== K3s and Helm preparations

==== K3s

include::guide-kubernetes-k3s.adoc[]

==== Helm

include::guide-kubernetes-helm.adoc[]

=== Book List API

[IMPORTANT]
In case there will be any problems with this service code please create issues ticket by using this link:
https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list/-/issues
and using button: btn:[New issue]. If you are on live workshop just ask a question.

ifdef::backend-html5[]
**TODO:** Video
endif::[]

This project needs to be cloned into `~/go/src/gitlab.com/mobica-workshops/examples/go/gorilla` folder in case
when `$GOPATH` is set to `~/go`. You will find two methods of cloning this project https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list[here].

include::guide-local-service-configuration.adoc[]

==== Service Development and Continuous Integration

include::guide-local-service-development-prefix.adoc[]

include::guide-local-book-list-service-development.adoc[]

include::guide-local-service-development-sufix.adoc[]

==== Continuous Delivery and end-to-end testing

include::guide-local-continuous-delivery-prefix.adoc[]

include::guide-local-book-list-continuous-delivery.adoc[]

include::guide-local-continuous-delivery-sufix.adoc[]

==== Continuous Security Testing

include::guide-local-book-list-security.adoc[]
include::guide-local-security-next-steps.adoc[]

==== Continuous Deployment Testing

include::guide-local-continuous-deployment-test.adoc[]

==== Extra: Multiplatform

include::guide-local-book-list-multiplatform.adoc[]
include::guide-local-multiplatform-next.adoc[]

=== Book Admin API

[IMPORTANT]
In case there will be any problems with this service code please create issues ticket by using this link:
https://gitlab.com/mobica-workshops/examples/go/gorilla/book-admin/-/issues
and using button: btn:[New issue]. If you are on live workshop just ask a question.

ifdef::backend-html5[]
**TODO:** Video
endif::[]

This project needs to be cloned into `~/go/src/gitlab.com/mobica-workshops/examples/go/gorilla` folder in case
when `$GOPATH` is set to `~/go`. You will find two methods of cloning this project https://gitlab.com/mobica-workshops/examples/go/gorilla/book-admin[here].

include::guide-local-service-configuration.adoc[]

==== Service Development and Continuous Integration

include::guide-local-service-development-prefix.adoc[]

include::guide-local-book-admin-service-development.adoc[]

include::guide-local-service-development-sufix.adoc[]

==== Continuous Delivery and end-to-end testing

include::guide-local-continuous-delivery-prefix.adoc[]

include::guide-local-book-admin-continuous-delivery.adoc[]

include::guide-local-continuous-delivery-sufix.adoc[]

==== Continuous Security Testing

include::guide-local-book-admin-security.adoc[]
include::guide-local-security-next-steps.adoc[]

==== Continuous Deployment Testing

include::guide-local-continuous-deployment-test-compact.adoc[]

==== Extra: Multiplatform

include::guide-local-book-admin-multiplatform.adoc[]
include::guide-local-multiplatform-next.adoc[]

=== Backend For Frontend

[IMPORTANT]
In case there will be any problems with this service code please create issues ticket by using this link:
https://gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/-/issues
and using button: btn:[New issue]. If you are on live workshop just ask a question.

ifdef::backend-html5[]
**TODO:** Video
endif::[]

This project needs to be cloned into `~/go/src/gitlab.com/mobica-workshops/examples/go/gorilla` folder in case
when `$GOPATH` is set to `~/go`. You will find two methods of cloning this project https://gitlab.com/mobica-workshops/examples/go/gorilla/book-bff[here].

[NOTE]
====
To sign in using swagger which is used in the next steps please use our test user: `atulodzi@gmail.com` with a test password: `NicePass4Me`
====

include::guide-local-service-configuration.adoc[]

==== Service Development and Continuous Integration

include::guide-local-service-development-prefix.adoc[]

include::guide-local-book-bff-service-development.adoc[]

include::guide-local-service-development-sufix.adoc[]

==== Mocks

include::guide-local-book-bff-mocks.adoc[]

==== Continuous Delivery and end-to-end testing

include::guide-local-continuous-delivery-prefix.adoc[]

include::guide-local-book-bff-continuous-delivery.adoc[]

include::guide-local-continuous-delivery-sufix.adoc[]

==== Continuous Security Testing

include::guide-local-book-bff-security.adoc[]
include::guide-local-security-next-steps.adoc[]

==== Continuous Deployment Testing

include::guide-local-continuous-deployment-test-compact.adoc[]

==== Extra: Multiplatform

include::guide-local-book-bff-multiplatform.adoc[]
include::guide-local-multiplatform-next.adoc[]

== What Next

=== Learning paths

After this workshop you can to take those learning paths:

- A Cloud Guru Containers and Kubernetes related trainings - where you can learn more about Docker, Kubernetes, and Helm
- A Cloud Guru DevOps automation related trainings - where you can learn more about Configuration Management tools like by example Ansible and many type of Pipelines

=== Next workshops in the microservices series

==== Frontend

For a Frontend developers we plan to release soon:

- Frontend local development with Docker and K3s for project using microservices architecture

It will be using **book-bff** which was shown at this workshops and will introduce JavaScript based front service **book-frontend**.

==== DevOps Automator and QA

For a DevOps Automator and QA engineers we plan to release:

- Continuous Integration, and Continuous Delivery/Deployment for project using microservices architecture with a help of the GITLAB platform. Delivery/Deployment will be targeting prepared K8s cluster.

It will be focusing mostly on Gitlab CI as an example of pipeline tool and what this tool can give us. Workshop is ready and a firs edition will be announced soon.

==== Infrastructure

For Cloud Infrastructure Engineers and SysOps:

- Preparing and using Infrastructure required for the projects using microservices architecture with the help of the Kubernetes, Clouds, Ansible, and Terraform.

Like in the title we will be creating mainly Cloud Infrastructure with a help of Terraform and other mentioned tools. It is possible that also bare metal will be mentioned and example prepared.

=== Related Open Source project:

We have also a special OpenSource project MOB175:

- Preparing examples used in the workshops series connected to the microservices architecture.

Anyone who is interested can join in a free time and people who are currently without project can let us know if wanted to join this project. Please contact me to gain more details.

== Cleanup

You can remove created K3d cluster created with this command:

[source,shell]
----
k3d cluster delete bookCluster
----

You can cleanup your docker engine with this command:

[source,shell]
----
docker system prune -a --volumes
----

All tools installed with a help of the `brew` can be uninstalled with the command `brew uninstall` plus `tool name`.

[source,shell]
----
brew uninstall tool-name
----

== QnA

Waiting for Questions :)

image:https://i.creativecommons.org/l/by/4.0/88x31.png[Creative Commons Licence,88,31] This work is licensed under a http://creativecommons.org/licenses/by/4.0/[Creative Commons Attribution 4.0 International License]
