[WARNING]
====
Configuration values storing method shown in those examples is also not the best solution. Please remember that for
an infrastructure related parts of the configuration, secrets, and things which changes often you should use tools like
Consulfootnote:[HashiCorp Consul https://www.consul.io/] and Vaultfootnote:[HashiCorp Vault https://www.vaultproject.io/].
====

For a simplicity of the examples our configuration for all environments will be stored in the project repository.
To show how to have at least minimum security with this concept we will be using in this example
Ansible Vaultfootnote:[Ansible Vault documentation https://docs.ansible.com/ansible/latest/user_guide/vault.html].
Problem is that for a simplicity we will not store this password in a secure way and for this *WARNING* bellow:

[CAUTION]
====
Our examples have password you need to use when running `local-configure.sh` in the `README.md` file.

NEVER EVER DO THIS!!!
====

Ansible which will be used to create configuration files will be run with a prepared script `local-configure.sh` this script looks
like this:

[source,bash]
----
#!/usr/bin/env bash
set -e
rm -Rf roles/secrets.*
ansible-galaxy install -r requirements.yml -p roles
ansible-playbook --vault-id @prompt -i 'localhost,' --connection=local local-playbook.yml
----

Current goal of the Ansible playbook this script runs is to generate service and helm configuration files for all
environments and store them in the `secrets` folder.

Ansible role used by the Ansible playbook is downloaded from the other location which can be checked in the `requirements.yml` file. This role is using in our examples values stored in the encrypted `local-vault.yml` file to generate mentioned upper files.

After using this script you will have those files in the mentioned `secrets` folder.

**Our Services configuration files:**

ci.env.yaml::
Service configuration which is used in the CI pipeline.

docker.env.yaml::
Service configuration which is also used in a local environment but for more complex testing - mainly by DevQA when they
are working on integration tests and end-to-end tests. It is also something what can be used to check if service
is deliverable.

k3s.env.yaml::
Service configuration which is used by local kubernetes called K3s. It will be used mainly by DevOps to check
if service is deployable.

prod.env.yaml::
Service configuration for our production environment

local.env.yaml::
Service configuration which is used for a local service development where Developer is actively developing service.

staging.env.yaml::
Service configuration for our staging environment. Environment which is used before production deployment.

**Our deployments Helm values files:**

k3s-values.yaml::
Values file used by our helm chart to deploy to our K3s local cluster.

prod-values.yaml::
Values file used by our helm chart to deploy to our production cluster.

staging-values.yaml::
Values file used by our helm chart to deploy to our staging cluster.

We will be analyzing those files during a Workshop.
