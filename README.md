# Developing microservices - BE with docker-compose and K3s

## Checklist v1.4

- [ ] use asciinema and/or OBS studio to record workshop session - or save with Google meet and process for later
- [ ] we need more task which everyone should make

## Checklist v1.3

- [ ] add [Bruno](https://www.usebruno.com/) as a replacement for postman and good example for E2E trsting
- [ ] add Nix/devbox https://nixos.org https://nix.dev https://jetify.com/devbox https://youtube.com/@DevOpsToolkit
- [ ] book-admin have a patch request (currently still put) - fix needed.
- [ ] move to `log/slog` from logrus
- [ ] Makefile example
- [ ] prometheus endpoint for backend microservice book-list
- [ ] prometheus endpoint for backend microservice book-admin
- [ ] prometheus endpoint for backend microservice book-bff
- [ ] add initContainer stage for backend microservice book-list
- [ ] add initContainer stage for backend microservice book-admin
- [ ] checking what type of core information we can include from microservices-architecture-the-complete-guide uDemy course created by Memi Lavi, and when we should suggest our trainees to learn using this course.
- [ ] adding syft as a CLI tool and Go library for generating a Software Bill of Materials (SBOM) from container images and filesystems. 
- [ ] create service-template in the go folder based on old service-template and our BE go microservices


## Checklist v1.2

- [X] extend readiness checklist section
- [X] we need to add ingress example: https://k3d.io/v5.3.0/usage/exposing_services/
- [X] explain basic of Security Scanning
- [X] explain Application Monitoring
- [X] add Sentry to examples
- [X] add multiplatform docker build example
- [X] example of running container security scans on the local environments
- [X] better explanation of why we are having some automation files and docker compose files in our projects
- [X] projects documentation should need only ruby to work
- [X] Documentation parts for a Windows users with WSL2 and brew type of installation
- [X] AWS Workspaces for attenders - REJECTED (to expensive)
- [X] Update doc to work with Ruby 3.3
- [X] Update docker compose files to remove deprecation warning

## Checklist v1.1

- [X] we need to add procedure to clean up own environment after workshop
- [X] we need to add using mock even for a BE workshop
- [X] we need to let everyone know how much time workshop will take and how many max people will be
- [X] we need some tips which part of BE services will be more explain on the other workshops as an add
- [X] we need at least a little help for a Windows users - Virtualbox image
- [X] we need to plan coffee brakes and lunch
- [X] we need some introduction document which will let us some basic knowledge about tools we plan to use and maybe materials to other courses connected to those tools - by example swagger - I think we need to extend document we have with requirements Issue #1
- [X] use NodePort instead of the ClusterIP for the K3s based deployments

## Checklist v1.0

- [x] backend microservice book-list
- [x] backend microservice book-admin 
- [x] backend microservice book-bff
- [x] mockup service which will be simulating book admin (book-list-mockup)
- [x] mockup service which will be simulating book admin (book-admin-mockup)
- [x] microservice-production-readiness-checklist
- [x] service-template which can be used as a starting code for any service (doc with the default documentation files and also standard files like Dockerfile and build.sh …)
- [x] development with  k3d
- [x] development with docker-compose
- [x] workshop microservices-part-1-backend-workshop
