* Start 9:00
* Section 1:
** Architecture
** Automation
** Configuration
** Service Skeleton
** Readiness Checklist
** Documentation
** Services
** Mockups
** Application Monitoring
** Application Security
* Coffee break 10:00 - 10:15
* Section 2:
** Book List API (demo + practice)
* Coffee break 11:30 - 11:45
* Section 3:
** Book Admin API (demo + practice)
* Lunch 12:30 - 13:00
* Section 4:
** Backend For Frontend (demo + practice)
* Coffee break 14:15 - 14:30
* Section 5:
** What's Next
** Cleanup
** Q&A
* Ends between 15:00 and 16:00 - depends on Q&A session