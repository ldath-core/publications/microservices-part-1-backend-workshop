#!/usr/bin/env bash
gem install bundler
bundle config --local github.https true
bundle config set --local path .bundle/gems && bundle
bundle exec asciidoctor-pdf -a allow-uri-read microservices-backend-local-development-guide.adoc
bundle exec asciidoctor microservices-backend-local-development-guide.adoc
