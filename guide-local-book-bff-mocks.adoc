In many cases you will be not using full existing infrastructure but mocked one. In our example our Book BFF service can use mocks instead of real services. This will be very important in the next workshops which will focus on the local development for the FrontEnd services. We have special docker compose file prepared to show this flow.

[source,bash]
----
docker-compose -f docker-compose-mocked.yml up -d
----

In our example we are using the most dumb mocks possible what means there will be no reaction in them on changes done by UPDATE and/or DELETE type of operations. It is not important and in most of the cases enough to develop BFF app and FrontEnd aps which will be using this service.

Please start again your application with `serve.sh` script :

[source,shell]
----
./serve.sh
----

Now just visit http://localhost:19010/[Swagger UI] in your browser and check if service is working properly.

When you will finish your work please stop your serving application with kbd:[Ctrl+C].

Please also turn off all containers when you will finish this section with command:

[source,bash]
----
docker-compose -f docker-compose-mocked.yml down
----
