Our target backend simple architecture will be looking like this:

ifdef::backend-html5[]
.Backend Architecture
[#img-backend-architecture]
[caption="Figure 1: "]
image::images/backend.svg[Backend Architecture]
endif::[]
ifndef::backend-html5[]
.Backend Architecture
[#img-backend-architecture]
[caption="Figure 1: "]
image::images/backend.png[Backend Architecture]
endif::[]


We have three backend microservices prepared which we will be using in our workshop:

- Our BFF Service will be simulated with https://gitlab.com/mobica-workshops/examples/go/gorilla/book-bff[Book API Gateway Service].
- Our API-1 Service will be simulated with https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list[Book List API Service].
- Our API-2 Service will be simulated with https://gitlab.com/mobica-workshops/examples/go/gorilla/book-admin[Book Admin API service]

In addition, our AP1-1 Service will have a mockup service prepared https://gitlab.com/mobica-workshops/examples/python/flask/book-list-mockup[Book List Mockup API Service],
and our API-2 Service will also have a mockup service prepared https://gitlab.com/mobica-workshops/examples/python/flask/book-admin-mockup[Book Admin Mockup API service].

We will be also introducing during this workshop a https://gitlab.com/mobica-workshops/templates/services/generic/standard-service[Service Template] which can be used to create new services in the similar way, and https://gitlab.com/mobica-workshops/examples/helm/book-service[Helm Chart] which will be used to deploy those services to the Kubernetes and K3s.

Because in this workshop we will be using GitlabCI we will introduce example of https://gitlab.com/mobica-workshops/templates/ci/gitlab-ci[CI Templates].

Another topics which are more and more important in the current workflow are Application Monitoring and Application Security. We will look at one of the basic Application Monitoring tool called Sentry and in case of Application Security we will focus only on the Container Scanning with tool called Trivy.
