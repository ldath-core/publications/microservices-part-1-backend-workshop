[.lead]
We will be discussing how our configuration works a little in the next section.

Local Configuration Management related scripts:

local-configure.sh::
This file is used to generate all configuration files we will be using in the examples for all environments we will be playing with in our example. It is part of our Infrastructure and Configuration Management automation and will be explained in the next section.

local-playbook.yml::
This is Ansible Playbook file used by `local-configure.sh`. It is part of our Infrastructure and Configuration Management automation and will be explained in the dedicated workshop session.

local-vault.yml::
File with variables for Ansible with encrypted by ansible vault secrets. It is part of our Infrastructure and Configuration Management automation and will be explained in the dedicated workshop session.

requirements.yml::
Ansible Galaxy requirements file. We are using it to show to ansible-galaxy from where we need ansible roles to be downloaded. This file is used by `local-configure.sh` and `gitlab-configure.sh` shell scripts. It is part of our Infrastructure and Configuration Management automation and will be explained in the dedicated workshop session.

Example responsibility table for it looks like this:

[cols="1,1,1,1"]
|===
|File name |Main responsibility|Helpers|Comment

|local-configure.sh
|DevOps
|Infrastructure, Developers, QA
|DevOps will create and maintain this file but Developers, QA, and Infrastructure needs to understand how it works as it will be used by them a lot

|local-playbook.yml
|DevOps
|Infrastructure, Developers, QA
|DevOps will create and maintain this file but Developers, QA, and Infrastructure needs to understand how it works as it will be used by them a lot

|local-vault.sh
|DevOps
|Infrastructure, Developers, QA
|This file stores our configuration variables and secrets and is mainly managed by DevOps but Infrastructure, Developers, and QA will be maintaining it too.

|requirements.yml
|DevOps
|Infrastructure, Developers
|This file is maintained by DevOps but Developers, QA, and Infrastructure needs to understand how it works
|===


[NOTE]
Our Ansible roles will be downloaded to the `roles` folder. Those roles need to be created and maintained by DevOps and Developers with a help from Infrastructure.


[WARNING]
====
We created our Configuration Management based on the Ansible for a good reason and a lot of things connected with it have meaning which is impossible to explain during this workshop and will be explained during the dedicated session for Infrastructure and DevOps.

When modifying this process for your own project please consult with us if you will think that something with it is not needed, to not lose time for adding it back later.
====